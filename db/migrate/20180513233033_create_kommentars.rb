class CreateKommentars < ActiveRecord::Migration[5.1]
  def change
    create_table :kommentars do |t|
      t.string :user
      t.text :body
      t.references :auftrag, foreign_key: true

      t.timestamps
    end
  end
end
