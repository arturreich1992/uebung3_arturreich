class Auftrag < ApplicationRecord
    has_many :kommentars, dependent: :destroy
     validates :title, presence: true,
                    length: { minimum: 5, maximum: 50 }
    
end
