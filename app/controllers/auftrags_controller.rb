class AuftragsController < ApplicationController
  before_action :set_auftrag, only: [:edit, :update, :show, :destroy]
  
  def index
    @auftrags = Auftrag.all
  end
  
  def show
  end
    
  def edit
  end

  def new
    @auftrag = Auftrag.new
  end
    
  def create
    @auftrag = Auftrag.new(auftrag_params)
    if @auftrag.save
      flash[:notice] ="Auftrags wurde erfolgreich erstellt"
      redirect_to @auftrag
    else
      render 'new'
    end
  end

  def update
    if @auftrag.update(auftrag_params)
      flash[:notice] = "Auftrag wurde erfolgreich geupdated"
      redirect_to @auftrag
    else
      render 'edit'
    end
  end
    
  def destroy
    @auftrag.destroy
    redirect_to auftrags_path
  end
    
    private
  def set_auftrag
  @auftrag = Auftrag.find(params[:id])
  end
  
  def auftrag_params
    params.require(:auftrag).permit(:title, :content)
  end
end
