class KommentarsController < ApplicationController
  def create
    @auftrag = Auftrag.find(params[:auftrag_id])
    @kommentar = @auftrag.kommentars.create(kommentar_params)
    redirect_to auftrag_path(@auftrag)
  end
  
  def destroy
    @auftrag = Auftrag.find(params[:auftrag_id])
    @kommentar = @auftrag.kommentars.find(params[:id])
    @kommentar.destroy
    redirect_to auftrag_path(@auftrag)
  end
 
  private
    def kommentar_params
      params.require(:kommentar).permit(:user, :body)
    end
end
