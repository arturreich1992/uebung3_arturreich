# README

#Uebung3_RubyonRails_AuftragUser

In diesem Ruby on Rails Projekt können sie Aufträge ihrer Wahl erstellen und Ihnen einen Content in Schriftlicher Form geben. Zudem ist es Usern erlaubt Aufträge zu kommentieren

##Benötigte Softwareumgebung:

* Ruby 2.4.1
* Rails 5.1.4

##Installation der Software

###rvm und ruby:
Je nach Betriebssytem unterscheidet sich die Installation der Softwareumgebung für dieses Projekt.
Dieses Projekt wurde auf Ubuntu erstellt und die Anleitung gilt daher auch hauptsächlich für Ubuntu Nutzer.

###Zunächst alles essentielle für Webpacker support:

* curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
* curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
* echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

* sudo apt-get update
* sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev nodejs yarn

##Nun installieren wird rvm und ruby:

* sudo apt-get install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
* gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
* curl -sSL https://get.rvm.io | bash -s stable
* source ~/.rvm/scripts/rvm
* rvm install 2.5.0

###Hier gehen wir sicher das wir die richtige ruby Version (2.4.1) als Standart benutzen.
* rvm use 2.4.1 --default

###Prüfen ob alles geklappt hat:
* ruby -v
* ruby 2.4.1 ...

###Zum Schluss dieses Teils wird der bundler für die gems Installiert:
* gem install bundler

##Installieren von Rails:

###Zunächst gehen wir sicher das wir NodeJS (optional aber empfehlenswert):
* curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
* sudo apt-get install -y nodejs

##Nun installieren wir Rails 5.1.4:
* gem install rails -v 5.1.4

###Prüfen ob alles funktioniert hat:
* rails -v 

Rails 5.1.4

##Ausführen des Projektes:

Über das Terminal/Commandozeile in das Projektverzeichnis wechseln (mit dem cd befehl):
* cd .../.../Projektverzeichnis
* app clonen aus dem bit repository
* bundle install --without production eingeben
* rails db:migrate für die Datenbank eingeben
* rails server starten und app testen unter localhost:3000
* oder unter http://uebung3-arturreichapp.herokuapp.com/ im browser aufrufen

##Programmfunktionen
* über die Homepage auf Meine App klicken
* neuen Auftrag erstellen oder Anzeigen lassen, Editieren, Löschen
* unter Edit können Aufträge verändert werden und Kommentare des Auftrags verwaltet werden (Löschen)
* unter Destroy wird der Auftrag und alle Kommentare der User gelöscht (nested)

